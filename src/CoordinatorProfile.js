import React, {useState} from 'react';

import "./profile.css"
import {Redirect} from "react-router-dom";

export default function CoordinatorProfile() {

    let content1 = {
        English: {
            text1: "Technical University of Cluj-Napoca",
            text2: "Address",
            text3: "Phone",
            text9: "Schedule",
            text10: "Monday",
            text11: "Tuesday",
            text12: "Friday",
            text13: "Saturday",
            text14: "Sunday",
            text15: "closed",
            text16: "Note",
            text17: "The MONDAY afternoon program is during the academic year, except for vacations.",
            text18: "Address",
        },
        Romanian: {
            text1: "Universitatea Tehnica din Cluj-Napoca",
            text2: "Adresa",
            text3: "Telefon",
            text9: "Orar",
            text10: "Luni",
            text11: "Marti",
            text12: "Vineri",
            text13: "Sambata",
            text14: "Duminica",
            text15: "inchis",
            text16: "Nota",
            text17: "Programul de LUNI dupa-masa este pe durata anului unversitar, cu exceptia vacantelor.",
            text18: "Adrese",
        }
    }

    let languageStoredInLocalStorage = localStorage.getItem("language");
    let [language, setLanguage] = useState(
        languageStoredInLocalStorage ? languageStoredInLocalStorage : "English"
    );

    let content;
    (language === 'Romanian')
        ? (content = content1.Romanian)
        : (content = content1.English)

    var tableBooks = document.getElementById('books');
    if (tableBooks != null)
        tableBooks.remove();

    const token = localStorage.getItem("token");
    if(!token){
        return <Redirect to="/"/>
    }

    return (
        <div>
        <div className="profil">
            <br/>
            <img className="image" src={require("./img/tiberiuMarita.jpg")} alt="Marita" />
                <h1><b>Tiberiu Marita</b></h1>
                <p className="title">Associate Professor</p>
                <p>{content.text1}</p>
                <br/>
                    <button className="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false" > Contact </button>
                    <div className="dropdown-menu">
                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-home"/> {content.text2}: <p>Str. C. Daicoviciu, No.15</p></a>
                        <a className="dropdown-item" href="#"><i className="fa fa-fw fa-phone"/> {content.text3}: <p>(+40 264)
                            401457</p></a>
                        <a className="dropdown-item" href="https://mail.yahoo.com/d/compose/4989693236?.intl=ro&.lang=ro-RO&.partner=none&.src=fp"><i className="fa fa-fw fa-envelope"/> Mail: <p>tiberiu.marita@cs.utcluj.ro</p></a>
                    </div>
        </div>
            <br/><hr/><br/>
            <footer>
                <div className="container-fluid padding">
                    <div className="row text-center">
                        <div className="col-md-4">
                            <img src={require("./img/loggo.png")}/>
                            <hr className="light"/>
                            <p> +40-(0)264-401218 </p>
                            <p> utcn@cs.utcluj.ro </p>
                            <p> G. Baritiu 26-28, Sala 48 </p>
                            <p> Cluj-Napoca, Cluj, Romania </p>
                        </div>
                        <div className="col-md-4">
                            <hr className="light"/>
                            <h5> {content.text9} </h5>
                            <hr className="light"/>
                            <p> {content.text10}: 08:00-9:00, 11:00-14:00, 16:00-19:00 </p>
                            <p> {content.text11}-{content.text12}: 08:00-09:00, 11:00-14:00 </p>
                            <p> {content.text13}-{content.text14}: {content.text15} </p>
                            <p><b>{content.text16}:</b> {content.text17} </p>
                        </div>
                        <div className="col-md-4">
                            <hr className="light"/>
                            <h5> {content.text18} </h5>
                            <hr className="light"/>
                            <p><b>B-dul Muncii:</b> 400641, Cluj-Napoca, Romania </p>
                            <p><b>Strada Memorandumului:</b> 400114, Cluj-Napoca, Romania </p>
                            <p><b>George Baritiu:</b> 400027, Cluj-Napoca, Romania </p>
                        </div>
                        <div className="col-12">
                            <hr className="light-100"/>
                            <h5>&copy; prezentariLicente.ro </h5>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    );
}
