import React from "react";

function Navigation(props) {

    let content;
    (props.language === 'Romanian')
        ? content = "Selectati limba preferata"
        : content = "Select the preferred language"

    return (
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
            <div className="container">
                <a className="navbar-brand" href="#">
                    {content}
                </a>
                <div className="language-select">
                    <select
                        className="custom-select"
                        value={props.language}
                        onChange={e => props.handleSetLanguage(e.target.value)}
                    >
                        <option value="English">English</option>
                        <option value="Romanian">Romanian</option>
                    </select>
                </div>
            </div>
        </nav>
    );
}

export default Navigation;
